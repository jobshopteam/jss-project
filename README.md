Projekt JSS
===========

Cilj projekta je razviti najbolji algoritam za Job-shop scheduling problem.

Za sada je implementiran tabu-search algoritam kojeg su opisali Nowicki i Smutnicki u svojem članku (vidi mapu `literatura`). 

---------------------------------------------------------------------------

Struktura repozitorija
----------------------

* `literatura` -- različiti članci vezani uz JSS problem
* `prezentacije` -- prezentacije za Nogo
* `src` -- naš kod, srce i duša ovog projekta :) (polako u nastajanju)
    * `src/jss-tabu-lib` -- kod za tabu search kojeg su opisali Nowicki i Smutnicki
    * `src/console` -- osnovni command-line program za testiranje algoritama
* `test` -- instance problema na kojima testiramo

Postavljanje projekta
---------------------

* nakon kloniranja repozitorija otvoriti projekt u Qt-u
* koristimo tabove za indentaciju pa je potrebno tako postaviti i Qt Creator
    * u `Projects` tabu otvoriti `Code Style` izbornik i napraviti novi stil koji koristi tabove umjesto space-eva
* u `Projects` > `Build & Run` moze se postaviti `Build directory` da pokazuje na `build/` mapu (i za `Debug` i za `Release` konfiguraciju)
* treba napraviti datoteku `.gitignore` i u nju staviti linije `.gitignore`, `jss-project.pro.user`, `build/` i ostale datoteke koje nije potrebno commitati u repozitorij
    * osnovna `.gitignore` datoteka se nalazi u `.gitignore.example`
* klik na `Build` u `Edit` tabu kada je selektirani glavni projekt bi trebao kompilirati i library-e i aplikaciju
* moguce je kompilirati svaki pod-projekt posebno desnim klikom na pod-projekt i klikom na `build`
* izvrsne datoteke i kompilirane biblioteke bi se trebale nalaziti u `build/` folderu

Benchmark problemi
----------------------

* koristimo Taillardove i Demirkolove instance, najbolje gornje i donje međe nalaze se [ovdje](http://optimizizer.com/jobshop.php)

Testiranje na studentu (za sad samo za tabu-search)
---------------------------------------------------

### Postavljanje parametara
* u direktoriju `test/cfg` nalaze se primjeri configuracija parametara za testiranje
* za stvaranje nove konfiguracije kopirajte neku već postojeću i modificirajte ju u tekstualnom editoru (mislim da su imena parametara jasna)

### Kopiranje potrebnih datoteka na student
* __UNIX/LINUX:__
    * nakon što ste napravili željene konfiguracije pozicionirajte se u `test/` direktorij
    * postavite pravila izvršavanja skripti `compress`: `chmod +x compress`
    * pokrenite skriptu: `./compress`
    * skripta generira datoteku `data.tar.gz`
    * spojite se na student pomocu `sftp` i kopirajte datoteke `decompress` i `data.tar.gz` na student
* __WINDOWS:__
   * napravite direktorij `test/src` i u njega kopirajte sve `.h` i `.cpp` datoteke iz `src/jss-tabu-lib` i `src/console`
   * spojite se na student pomoću nekog FTP programa, npr _FileZilla_ i kopirajte direktorije `test/cfg`, `test/src`, `test/taillard`, `test/demirkol` i datoteke `test/compile` i `test/test`

### Pokretanje skripte za testiranje
* spojite se na student sa `ssh` / `putty`
* možete saznati koja su računala uključena s `ruptime | grep up`
* spojite se na željeno računalo (trenutno koristimo računala `pr2-03`, `pr2-09` i `pr2-15`) sa `ssh IME_RAČUNALA`
* naredbom `ps a -F` možete vidjeti informacije o procesima na tom računalu
    * u stupcu `PSR` je informacija o jezgri procesora na kojoj se proces izvršava
    * u stupcu `C` nalazi se postotak jezgre kojeg zauzima proces
* kopirajte sve datoteke koje ste prebacili na studenta iz mape `HOME` u neku mapu na računalu (da izbjegnete komunikaciju studenta i racunala) i pozicionirajte se u tu mapu
* __WINDOWS:__
    * postavite prava izvršavanja s `chmod +x compile` i `chmod +x test`
    * pozovite skriptu `./compile` i malo pricekajte
* __UNIX/LINUX:__
    * postavite prava izvršavanja s `chmod +x decompress`
    * pozovite skriptu `./decompress`
    * pozovite skriptu `./compile`
* pokrenite program `byobu`, opis korištenja:
    * `F2` -- stvaranje novog tab-a
    * `F3` i `F4` -- promjena trenutnog tab-a
    * `F6` -- detach (programi koji su pokrenuti u tabovima se nastavljaju izvršavati a vi se možete odlogirati)
* otvorite toliko tabova koliko primjera želite testirati, tj. koliko imate slobodnih jezgara
* u svakom tabu pokrenite skriptu `./test CONFIG INSTANCES COREMASK [TAG]`
    * _CONFIG_ - ime (bez ekstenzije) konfiguracijske datoteke koju ste kreirali
    * _INSTANCES_ - grupa instanci na kojima želite testirati (`demirkol`, `taillard` ili `"demirkol taillard"`)
    * _COREMASK_ - maska jezgre na kojoj zelite testirati program, maske su slijedece: (core0 - `0x1`, core1 - `0x2`, core2 -`0x4`, core3 - `0x8`)
    * _TAG_ - sufiks mape u koju će se spremiti rezultati (točnije, spremiti će se u mapu `test-DATUM-TAG`
    * npr. ako ste napraviti konfiguraciju `cfg/mojaKonfiguracija.cfg` i želite testirati na svim instancama na jezgri 1, pokrenite `./test mojaKonfiguracija "taillard demirkol" 0x2 mojaKonfiguracija` -- rezultati ce se (danas) spremiti u mapu `test-2015-02-28-mojaKonfiguracija` 
* pritiskom na tipku `F6` detachajte se od otvorenih terminala i odlogirajte se s računala i studenta
* ako vas zanima kako napreduje računanje, opet se spojite na studenta i računalo i pokrenite `byobu` -- vaši tabovi će još uvijek biti tamo

