TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
	src/jss-tabu-lib \
	src/jss-gen-lib \
	src/console \
	src/debug  \
	src/gui
