#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include <JssTabuBack.h>
#include <JssGen.h>
#include <JssHybrid.h>


using namespace std;

int main(int argc, char *argv[]) {
	time_t seed;
	clock_t startAlgo, startIter, endIter;

	srand(time(&seed));

	if (argc < 5) {
		cout << "Usage: " << argv[0] << " filename filetype outfile tabu|gen|hybrid [algorithm_params]\n";
		return 0;
	}
	ifstream fin(argv[1]);
	ofstream fout(argv[3]);

	string ftype = argv[2];
	jss::JssBase::InputType filetype;
	if (ftype == "Taillard" || ftype == "taillard") {
		filetype = jss::JssBase::Taillard;
	} else if (ftype == "Demirkol" || ftype == "demirkol") {
		filetype = jss::JssBase::Demirkol;
	} else {
		filetype = jss::JssBase::Custom;
	}

	string algorithm = argv[4];

	/*********************************************************************************************************/
	/************************************************ TABU ***************************************************/
	/*********************************************************************************************************/
	if (algorithm == "tabu" || algorithm == "Tabu") {
		jss::JssTabuParams params;

		if (argc == 10) {
			jss::JssTabuParams _params(argv[5], argv[6], argv[7], argv[8], argv[9]);
			params = _params;
		} else if (argc != 5) {
			cout << "Usage: " << argv[0] << " filename filetype outfile 'tabu' [tabuListSize iters maxL maxD maxC]\n";
			return 0;
		}

		startAlgo = clock();
		jss::JssTabuBack jobShop(fin, filetype, params);

		jss::JssBase::Time currentBest = jobShop.getBestMakespan();
		cout << "Starting makespan: " << jobShop.getBestMakespan() << endl;
		int totalIters = 1;

		startIter = clock();
		while(jobShop.nextIteration()) {
			jss::JssBase::Time tmp = jobShop.getBestMakespan();
			if (tmp < currentBest) {
				currentBest = tmp;
				cout << "Better solution found: " << currentBest << " (iteration: " << totalIters << ")\n";
				cout << "Iteration: " << totalIters << "      \r";
				cout.flush();
			} else if ((totalIters - 1) % 100 == 0) {
				cout << "Iteration: " << totalIters << "      \r";
				cout.flush();
			}
			++totalIters;
		}
		endIter = clock();

		cout << "Best makespan: " << jobShop.getBestMakespan() << " (iteration: " << totalIters << ")\n";
		cout << "Writing run data to " << argv[3] << ".\n";

		fout << "ALGORITHM=" << algorithm << endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << argv[1] << endl
			 << "INPUT_TYPE=" << argv[2] << endl
			 << "PARAMETERS={TL_SIZE=" << params.tabuMax
			 << ",ITERS=" << params.iterMax << ",BJ_SIZE=" << params.maxL << ",MAX_D=" << params.maxD
			 << ",MAX_C=" << params.maxC << ",SEED=" << seed << "}\n\n"
			 << "RESULTS:\n\n"
			 << "ITERATIONS=" << totalIters << endl
			 << "TOTAL_TIME=" << double(endIter - startAlgo) / CLOCKS_PER_SEC << endl
			 << "TIME_PER_ITER=" << double(endIter - startIter) / totalIters / CLOCKS_PER_SEC << endl
			 << "MAKESPAN=" << jobShop.getBestMakespan() << endl
			 << "SCHEDULE=\n";

		jss::JssBase::Schedule schedule = jobShop.getBestSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << endl;
		}
	}
	/*********************************************************************************************************/
	/************************************************ GENETIC ************************************************/
	/*********************************************************************************************************/
	else if (algorithm == "gen" || algorithm == "Gen") {

		jss::JssGenParams params;

		if (argc == 10) {
			jss::JssGenParams _params(argv[5], argv[6], argv[7], argv[8], argv[9]);
			params = _params;
		} else if (argc != 5) {
		   cout << "Usage: " << argv[0] << " filename filetype outfile 'gen' [pop_size per_top per_bottom p_cross max_g]\n";
		   return 0;
		}

		startAlgo = clock();

		jss::JssGen jobShop(fin, filetype, params);

		jss::JssBase::Time currentBest = jobShop.getBestMakespan();
		cout << "Starting makespan: " << jobShop.getBestMakespan() << endl;
		int totalIters = 1;
		int bestIteration = 1;
		startIter = clock();

		while (jobShop.nextGeneration()) {
			jss::JssBase::Time tmp = jobShop.getBestMakespan();
			if (tmp < currentBest) {
				currentBest = tmp;
				bestIteration = totalIters;
				cout << "Better solution found: " << currentBest << " (generation: " << totalIters << ")\n";
				cout << "Generation: " << totalIters << "      \r";
				cout.flush();
			} else {
				cout << "Generation: " << totalIters << "      \r";
				cout.flush();
			}
			++totalIters;
		}

		endIter = clock();

		cout << "Best makespan: " << jobShop.getBestMakespan() << " (generation: " << totalIters << ")\n";
		cout << "Writing run data to " << argv[3] << ".\n";

		fout << "ALGORITHM=" << algorithm << endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << argv[1] << endl
			 << "INPUT_TYPE=" << argv[2] << endl
			 << "PARAMETERS={POP_SIZE=" << params.pop_size << ",PER_TOP=" << params.per_top
			 << ",PER_BOTTOM=" << params.per_bottom << ",P_CROSS=" << params.p_cross
			 << ",MAX_G=" << params.max_g << ",SEED=" << seed << "}\n\n"
			 << "RESULTS:\n\n"
			 << "TOTAL_TIME=" << double(endIter - startAlgo) / CLOCKS_PER_SEC << endl
			 << "TIME_PER_GEN=" << double(endIter - startIter) / (totalIters - 1) / CLOCKS_PER_SEC << endl
			 << "MAKESPAN=" << jobShop.getBestMakespan() << endl
			 << "GENERATION=" << bestIteration << endl
			 << "SCHEDULE=\n";

		jss::JssBase::Schedule schedule = jobShop.getBestSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << endl;
		}
	}
	/********************************************************************************************************/
	/************************************************ HYBRID ************************************************/
	/********************************************************************************************************/
	else if (algorithm == "hybrid" || algorithm == "Hybrid") {

		jss::JssGenParams gen_params("-1", "0.1", "0.2", "0.7", "40");
		jss::JssTabuParams inner_tabu_params=jss::JssTabuParams("10","2","0","10","2");
		jss::JssTabuParams finish_tabu_params=jss::JssTabuParams();

		switch (argc) {
		case 20: {
				jss::JssTabuParams _finish_tabu_params(argv[15], argv[16], argv[17], argv[18], argv[19]);
				finish_tabu_params = _finish_tabu_params;
			}
		case 15: {
				jss::JssTabuParams _inner_tabu_params(argv[10], argv[11], argv[12], argv[13], argv[14]);
				inner_tabu_params = _inner_tabu_params;
			}
			// NO break
		case 10: {
				jss::JssGenParams _gen_params(argv[5], argv[6], argv[7], argv[8], argv[9]);
				gen_params = _gen_params;
			}
		case 5:
			break;
		default: {
				cout << "Usage: " << argv[0] << " filename filetype outfile 'hybrid' [pop_size per_top per_bottom p_cross max_g"
					 << "[tabuListSize iters maxL maxD maxC "
					 << "[tabuListSize iters maxL maxD maxC]]]\n";
				return 0;
			}
		}

		startAlgo = clock();

		jss::JssHybrid jobShop(fin, filetype, gen_params, inner_tabu_params);

		startIter = clock();

		jss::JssBase::Time currentBest = jobShop.getBestMakespan();
		cout << "Starting makespan: " << jobShop.getBestMakespan() << endl;
		int totalIters = 1;
		int bestIter = 1;

		while (jobShop.nextGeneration()) {
			jss::JssBase::Time tmp = jobShop.getBestMakespan();
			if (tmp < currentBest) {
				currentBest = tmp;
				bestIter = totalIters;
				cout << "Better solution found: " << currentBest << " (generation: " << totalIters << ")\n";
				cout << "Generation: " << totalIters << "         \r";
				cout.flush();
			} else {
				cout << "Generation: " << totalIters << "         \r";
				cout.flush();
			}
			++totalIters;
		}

		endIter = clock();

		//jobShop.finishingTouch(finish_tabu_params);

		jss::JssTabuGen jssFinish = jobShop.finishingTouch(finish_tabu_params);

		jss::JssBase::Time currentBestFinish = jssFinish.getBestMakespan();
		std::cout << "Starting finishing touch" << std::endl;
		int totalItersFinish = 1;
		while(jssFinish.nextIteration()) {
			jss::JssBase::Time tmp = jssFinish.getBestMakespan();
			if (tmp < currentBestFinish) {
				currentBestFinish = tmp;
				std::cout << "Better solution found: " << currentBestFinish << " (iteration: " << totalItersFinish << ")\n";
				std::cout << "Iteration: " << totalItersFinish << "\r";
				std::cout.flush();
			} else if ((totalItersFinish - 1) % 100 == 0) {
				std::cout << "Iteration: " << totalItersFinish << "\r";
				std::cout.flush();
			}
			++totalItersFinish;
		}

		//s_data[bestSchedule] = jssFinish.getSchedule();
		//best_fitness = jssFinish.getBestMakespan();


		cout << "Best makespan: " << jssFinish.getBestMakespan() << " (generation: " << totalItersFinish << ")\n";
		cout << "Writing run data to " << argv[3] << ".\n";

		fout << "ALGORITHM=" << algorithm << endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << argv[1] << endl
			 << "INPUT_TYPE=" << argv[2] << endl
			 << "PARAMETERS={POP_SIZE=" << gen_params.pop_size << ",PER_TOP=" << gen_params.per_top
			 << ",PER_BOTTOM=" << gen_params.per_bottom << ",P_CROSS=" << gen_params.p_cross
			 << ",MAX_G=" << gen_params.max_g << ",INNER={TL_SIZE=" << inner_tabu_params.tabuMax << ",ITERS=" << inner_tabu_params.iterMax
			 << ",BJ_SIZE=" << inner_tabu_params.maxL << ",MAX_D=" << inner_tabu_params.maxD
			 << ",MAX_C=" << inner_tabu_params.maxC << "},FINISH={TL_SIZE=" << finish_tabu_params.tabuMax << ",ITERS=" << finish_tabu_params.iterMax
			 << ",BJ_SIZE=" << finish_tabu_params.maxL << ",MAX_D=" << finish_tabu_params.maxD
			 << ",MAX_C=" << finish_tabu_params.maxC << "},SEED=" << seed << "}\n\n"
			 << "RESULTS:\n\n"
			 << "MAKESPAN=" << jssFinish.getBestMakespan() << endl
			 << "GENERATION=" << bestIter << endl
			 << "SCHEDULE=\n";

		jss::JssBase::Schedule schedule = jssFinish.getSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << endl;
		}
	}
	else {
		cout << "Usage: " << argv[0] << " filename filetype outfile tabu|gen|hybrid [algorithm_params]\n";
		return 0;
	}

	return 0;
}
