QT -= gui
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

DESTDIR = ../../

OBJECTS_DIR = $$DESTDIR/obj/debug
MOC_DIR = $$DESTDIR/moc/debug
RCC_DIR = $$DESTDIR/rcc/debug
UI_DIR = $$DESTDIR/ui/debug

#include(deployment.pri)
#qtcAddDeployment()

LIBS += -L$$DESTDIR -ljss-tabu-lib -ljss-gen-lib

unix:!mac {
	QMAKE_LFLAGS += -Wl,-rpath=.
}

INCLUDEPATH += $$PWD/../jss-tabu-lib
INCLUDEPATH += $$PWD/../jss-gen-lib
DEPENDPATH += $$PWD/../jss-tabu-lib
DEPENDPATH += $$PWD/../jss-gen-lib

SOURCES += \
	main.cpp

unix:QMAKE_CXXFLAGS += -Wno-sign-compare -O3
