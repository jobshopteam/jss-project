#ifndef GANTCHART_H
#define GANTCHART_H

#include <QtQuick/QQuickPaintedItem>
#include <QColor>

#include "JssScheduleWrapper.h"

class GantChart : public QQuickPaintedItem {
	Q_OBJECT
	Q_PROPERTY(JssScheduleWrapper* schedule READ schedule WRITE setSchedule)
	Q_PROPERTY(qreal margin READ margin WRITE setMargin)
	Q_PROPERTY(qreal boxHeight READ boxHeight WRITE setBoxHeight)
	Q_PROPERTY(qreal spacing READ spacing WRITE setSpacing)
	Q_PROPERTY(qreal legendHeight READ legendHeight WRITE setLegendHeight)
	Q_PROPERTY(int gridLabels READ gridLabels WRITE setGridLabels)
public:
	GantChart(QQuickItem *parent = NULL) : QQuickPaintedItem(parent), m_schedule(NULL),
		m_margin(15), m_boxHeight(15), m_spacing(5), m_legendHeight(15), m_gridLabels(10) {}

	JssScheduleWrapper* schedule() const { return m_schedule; }
	void setSchedule(JssScheduleWrapper *schedule) { m_schedule = schedule; update(); }
	qreal margin() const { return m_margin; }
	void setMargin(qreal margin) { m_margin = margin; }
	qreal boxHeight() const { return m_boxHeight; }
	void setBoxHeight(qreal boxHeight) { m_boxHeight = boxHeight; }
	qreal spacing() const { return m_spacing; }
	void setSpacing(qreal spacing) { m_spacing = spacing; }
	qreal legendHeight() const { return m_legendHeight; }
	void setLegendHeight(qreal legendHeight) { m_legendHeight = legendHeight; }
	int gridLabels() const { return m_gridLabels; }
	void setGridLabels(int gridLabels) { m_gridLabels = gridLabels; }

	void paint(QPainter *painter);

private:
	JssScheduleWrapper *m_schedule;
	qreal m_margin;
	qreal m_boxHeight;
	qreal m_spacing;
	qreal m_legendHeight;
	int m_gridLabels;
};

#endif // GANTCHART_H
