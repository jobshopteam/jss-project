#ifndef JSSENGINE_H
#define JSSENGINE_H

#include <QtQuick/QQuickItem>
#include <QRunnable>
#include <QUrl>

#include <cstdlib>
#include <ctime>

#include <JssTabuBack.h>
#include <JssGen.h>
#include <JssHybrid.h>

#include "JssScheduleWrapper.h"

class JssEngine : public QQuickItem {
	Q_OBJECT
	// informacije o tijeku izvodjenja
	Q_PROPERTY(JssScheduleWrapper* schedule READ schedule NOTIFY scheduleChanged)
	Q_PROPERTY(int makespan READ makespan NOTIFY scheduleChanged)
	Q_PROPERTY(int totalIterations READ totalIterations NOTIFY iterationCompleted)
	Q_PROPERTY(int totalGenerations READ totalGenerations NOTIFY generationCompleted)
	Q_PROPERTY(int status READ status NOTIFY statusChanged)

	// ulazne datoteke
	Q_PROPERTY(QUrl inputFile READ inputFile WRITE setInputFile)
	Q_PROPERTY(QString inputType READ inputType WRITE setInputType)
	Q_PROPERTY(QUrl outputFile READ outputFile WRITE setOutputFile)
	// tip algoritma
	Q_PROPERTY(QString algorithm READ algorithm WRITE setAlgorithm)
	// opcije za tabu
	Q_PROPERTY(int tabuSize READ tabuSize WRITE setTabuSize)
	Q_PROPERTY(int iterations READ iterations WRITE setIterations)
	Q_PROPERTY(int backJumpSize READ backJumpSize WRITE setBackJumpSize)
	Q_PROPERTY(int cycleSize READ cycleSize WRITE setCycleSize)
	Q_PROPERTY(int cycleRepetition READ cycleRepetition WRITE setCycleRepetition)
	// opcije za genetski
	Q_PROPERTY(int generations READ generations WRITE setGenerations)
	Q_PROPERTY(qreal eliteRate READ eliteRate WRITE setEliteRate)
	Q_PROPERTY(qreal extinctRate READ extinctRate WRITE setExtinctRate)
	Q_PROPERTY(qreal crossRate READ crossRate WRITE setCrossRate)
	Q_PROPERTY(int populationSize READ populationSize WRITE setPopulationSize)
	// opcije za hybrid
	Q_PROPERTY(int hTabuSize READ hTabuSize WRITE setHTabuSize)
	Q_PROPERTY(int hIterations READ hIterations WRITE setHIterations)
	Q_PROPERTY(int hBackJumpSize READ hBackJumpSize WRITE setHBackJumpSize)
	Q_PROPERTY(int hCycleSize READ hCycleSize WRITE setHCycleSize)
	Q_PROPERTY(int hCycleRepetition READ hCycleRepetition WRITE setHCycleRepetition)

private:
	class JssRunable : public QRunnable {
		friend class JssEngine;
	private:
		JssEngine *parent;
	public:
		JssRunable(JssEngine *parent) : parent(parent) {}
		void run() { parent->execute(); }
	};

public:
	JssEngine(QQuickItem *parent = NULL) : QQuickItem(parent),
		tabuSearch(NULL), genetic(NULL), hybrid(NULL), tabuGen(NULL),
		m_schedule(NULL), m_inputFile(""), m_inputType("custom"), m_outputFile(""),
		m_algorithm("tabu"), m_tabuSize(10), m_iterations(3500), m_backJumpSize(5),
		m_cycleSize(100), m_cycleRepetition(2),
		m_generations(100), m_eliteRate(0.1), m_extinctRate(0.2), m_crossRate(0.7),
		m_populationSize(-1),
		m_bestMakespan(0), m_totalIterations(0), m_totalGenerations(0),
		pauseRequest(false), stopRequest(false), m_status(0) {
		srand(time(NULL));
	}

	~JssEngine();

	JssScheduleWrapper* schedule() const { return m_schedule; }
	int makespan() const { return m_bestMakespan; }
	int totalIterations() const { return m_totalIterations; }
	int totalGenerations() const { return m_totalGenerations; }
	int status() const { return m_status; }

	QUrl inputFile() const { return m_inputFile; }
	QString inputType() const { return m_inputType; }
	QUrl outputFile() const { return m_outputFile; }
	QString algorithm() const { return m_algorithm; }
	int tabuSize() const { return m_tabuSize; }
	int iterations() const { return m_iterations; }
	int backJumpSize() const { return m_backJumpSize; }
	int cycleSize() const { return m_cycleSize; }
	int cycleRepetition() const { return m_cycleRepetition; }
	int generations() const { return m_generations; }
	qreal eliteRate() const { return m_eliteRate; }
	qreal extinctRate() const { return m_extinctRate; }
	qreal crossRate() const { return m_crossRate; }
	int populationSize() const { return m_populationSize; }
	int hTabuSize() const { return m_hTabuSize; }
	int hIterations() const { return m_hIterations; }
	int hBackJumpSize() const { return m_hBackJumpSize; }
	int hCycleSize() const { return m_hCycleSize; }
	int hCycleRepetition() const { return m_hCycleRepetition; }

	void setInputFile(QUrl inputFile) { m_inputFile = inputFile; }
	void setInputType(QString inputType) { m_inputType = inputType; }
	void setOutputFile(QUrl outputFile) { m_outputFile = outputFile; }
	void setAlgorithm(QString algorithm) { m_algorithm = algorithm; }
	void setTabuSize(int tabuSize) { m_tabuSize = tabuSize; }
	void setIterations(int iterations) { m_iterations = iterations; }
	void setBackJumpSize(int backJumpSize) { m_backJumpSize = backJumpSize; }
	void setCycleSize(int cycleSize) { m_cycleSize = cycleSize; }
	void setCycleRepetition(int cycleRepetition) { m_cycleRepetition = cycleRepetition; }
	void setGenerations(int generations) { m_generations = generations; }
	void setEliteRate(qreal eliteRate) { m_eliteRate = eliteRate; }
	void setExtinctRate(qreal extinctRate) { m_extinctRate = extinctRate; }
	void setCrossRate(qreal crossRate) { m_crossRate = crossRate; }
	void setPopulationSize(int populationSize) { m_populationSize = populationSize; }
	void setHTabuSize(int hTabuSize) { m_hTabuSize = hTabuSize; }
	void setHIterations(int hIterations) { m_hIterations = hIterations; }
	void setHBackJumpSize(int hBackJumpSize) { m_hBackJumpSize = hBackJumpSize; }
	void setHCycleSize(int hCycleSize) { m_hCycleSize = hCycleSize; }
	void setHCycleRepetition(int hCycleRepetition) { m_hCycleRepetition = hCycleRepetition; }

	Q_INVOKABLE void start();
	Q_INVOKABLE void pause();
	Q_INVOKABLE void resume();
	Q_INVOKABLE void stop();

signals:
	void scheduleChanged();
	void statusChanged();
	void iterationCompleted();
	void generationCompleted();
	void inputError();

private:
	void execute();
	void executeTabu();
	void executeGenetic();
	void executeHybrid();
	void executeTabuGen();
	void saveToFile();

	jss::JssTabuBack *tabuSearch;
	jss::JssGen *genetic;
	jss::JssHybrid *hybrid;
	jss::JssTabuGen *tabuGen;

	jss::JssTabuParams lastTabuParams;
	jss::JssTabuParams lastInternalTabuParams;
	jss::JssGenParams lastGenParams;

	JssScheduleWrapper *m_schedule;
	QUrl m_inputFile;
	QString m_inputType;
	QUrl m_outputFile;
	QString m_algorithm;

	int m_tabuSize;
	int m_iterations;
	int m_backJumpSize;
	int m_cycleSize;
	int m_cycleRepetition;

	int m_generations;
	qreal m_eliteRate;
	qreal m_extinctRate;
	qreal m_crossRate;
	int m_populationSize;

	int m_hTabuSize;
	int m_hIterations;
	int m_hBackJumpSize;
	int m_hCycleSize;
	int m_hCycleRepetition;

	int m_bestMakespan;
	int m_totalIterations;
	int m_totalGenerations;

	bool pauseRequest;
	bool stopRequest;
	int m_status;
};

#endif // JSSENGINE_H
