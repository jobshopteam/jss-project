#include "JssScheduleWrapper.h"

JssScheduleWrapper::JssScheduleWrapper(const jss::JssBase::Schedule &schedule,
									   const std::vector<jss::JssBase::Job> &jobList) {
	m_schedule.resize(schedule.size());
	for (int i = 0; i < m_schedule.size(); ++i) {
		m_schedule[i].resize(schedule[i].size());
		for (int j = 0; j < m_schedule[i].size(); ++j) {
			m_schedule[i][j] = Operation(schedule[i][j], schedule[i][j] + jobList[i][j].time, jobList[i][j].machine);
		}
	}
}
