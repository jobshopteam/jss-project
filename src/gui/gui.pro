TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
	GantChart.cpp \
	JssScheduleWrapper.cpp \
	JssEngine.cpp

RESOURCES += qml.qrc

DESTDIR = ../../

OBJECTS_DIR = $$DESTDIR/obj/gui
MOC_DIR = $$DESTDIR/moc/gui
RCC_DIR = $$DESTDIR/rcc/gui
UI_DIR = $$DESTDIR/ui/gui

LIBS += -L$$DESTDIR -ljss-tabu-lib -ljss-gen-lib

unix:!mac {
	QMAKE_LFLAGS += -Wl,-rpath=.
}

unix: QMAKE_CXXFLAGS += -Wno-sign-compare -O3

INCLUDEPATH += $$PWD/../jss-tabu-lib
INCLUDEPATH += $$PWD/../jss-gen-lib
DEPENDPATH += $$PWD/../jss-tabu-lib
DEPENDPATH += $$PWD/../jss-gen-lib

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES +=

HEADERS += \
	GantChart.h \
	JssScheduleWrapper.h \
	JssEngine.h
