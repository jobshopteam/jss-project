#include <iostream>

#include "JssGen.h"

#include <vector>
#include <algorithm>    // std::sort
#include <cstdlib>     /* srand, rand */
#include <ctime>       /* time */
#include <climits>

namespace jss {

//koristeno samo za sortiranje - vjerojatno ce bit potrebno maknuti kod optimizacije

bool chrCmp(std::pair<JssGen::Chromosome, int>* a, std::pair<JssGen::Chromosome, int>* b) {
	return a->second < b->second;
}

JssGen::JssGen(std::istream &is, InputType inputType, JssGenParams params, JssTabuParams tabuParams) :
		JssBase(is, inputType), population(0),
		tmpPop(1), tabuParams(tabuParams), bestSchedule(0), tmpSchedule(1) {
	if (params.pop_size == -1) {
		pop_size = 2 * getNumberOfOperations();
	} else {
		pop_size = params.pop_size;
	}
	chr_size = 2 * getNumberOfOperations();
	this->max_g = params.max_g;
	this->p_cross = params.p_cross;

	noOfElites = (int)(params.per_top * pop_size);
	noOfMutants = (int)(params.per_bottom * pop_size);
	noOfCrossovers = pop_size - (noOfElites + noOfMutants);

	popData[population].resize(pop_size);
	popData[tmpPop].resize(pop_size);
	for (int i = 0; i < pop_size; ++i) {
		popData[population][i] = new std::pair<Chromosome, int>(Chromosome(chr_size), -1);
		popData[tmpPop][i] = new std::pair<Chromosome, int>(Chromosome(chr_size), -1);
	}
	generateInitialPopulation();
	evaluatePopulation();

	gen = 1;
}

JssGen::~JssGen() {
	for (int i = 0; i < popData[population].size(); ++i) {
		delete popData[population][i];
		delete popData[tmpPop][i];
	}
}

const JssGen::Chromosome& JssGen::run() {
	generateInitialPopulation();
	gen = 1;
	while (nextGeneration());
	return best_chromosome;
}

bool JssGen::nextGeneration() {
	if (gen >= max_g) {
		return false;
	}
	++gen;

	//std::vector<std::pair<Chromosome, int> > children;

	std::nth_element(popData[population].begin(), popData[population].begin() + noOfElites,
					 popData[population].end(), chrCmp);

	// elitizam
	for(int i = 0; i < noOfElites; ++i) {
		*(popData[tmpPop][i]) = *(popData[population][i]);
	}

	// krizanje
	for (int i = noOfElites; i < noOfElites + noOfCrossovers; ++i) {
		Chromosome &child = popData[tmpPop][i]->first;
		const Chromosome &parent1 = kTournament(3);
		const Chromosome &parent2 = kTournament(3);

		for(int j = 0; j < chr_size; ++j){
			if((double)rand() / RAND_MAX < p_cross) {
				child[j] = parent1[j];
			} else {
				child[j] = parent2[j];
			}
		}
		popData[tmpPop][i]->second = -1;
	}

	// mutacija
	for(int i = noOfElites + noOfCrossovers; i < pop_size; ++i) {
		Chromosome &mutant = popData[tmpPop][i]->first;
		for(int j = 0; j < chr_size; ++j) {
			mutant[j] = double(rand()) / RAND_MAX;
		}
		popData[tmpPop][i]->second = -1;
	}

	std::swap(population, tmpPop);

	evaluatePopulation();

	return true;
}

void JssGen::generateInitialPopulation() {
	best_fitness = -1;
	for(int i = 0; i < pop_size; ++i) {
		for(int j = 0; j < chr_size; ++j) {
			popData[population][i]->first[j] = double(rand()) / RAND_MAX;
		}
		popData[population][i]->second = -1;
	}
}

void JssGen::evaluatePopulation() {
	for(int i = 0; i < pop_size; ++i) {
		if (popData[population][i]->second == -1) {
			popData[population][i]->second = fitness(popData[population][i]->first, s_data[tmpSchedule]);
			if (best_fitness == -1 || popData[population][i]->second < best_fitness) {
				best_fitness = popData[population][i]->second;
				std::swap(tmpSchedule, bestSchedule);
				best_chromosome = (popData[population][i]->first);
			}
		}
	}
}

JssTabuGen JssGen::decode(const Chromosome& c) const {
	JssTabuGen tabu_gen(c, jobList, getNumberOfMachines(), tabuParams);
	return tabu_gen;
}

const JssGen::Chromosome& JssGen::kTournament(int k) {
	Chromosome *best = NULL;
	int best_fit = 0;

	for (int i = 0; i < k; i++) {
		int ind = rand() % pop_size;
		Chromosome& cand = popData[population][ind]->first;
		int cand_fit = popData[population][ind]->second;
		if (i == 0 || cand_fit < best_fit) {
			best_fit = cand_fit;
			best = &cand;
		}
	}

	return *best;
}

int JssGen::fitness(const Chromosome &c, Schedule &s) const {
	jss::JssTabuGen jss = decode(c);
	jss.localSearch();
	s = jss.getBestSchedule();
	return jss.getBestMakespan();
}

int JssGen::fitness1(const Chromosome& c, Schedule &s) const {
	jss::JssTabuGen jss = decode(c);
	jss.nextIteration();
	jss.nextIteration();
	s = jss.getBestSchedule();
	return jss.getBestMakespan();
}

int JssGen::fitness2(const Chromosome& c, Schedule &s) const {
	jss::JssTabuGen jss = decode(c);
	while(jss.nextIteration());
	s = jss.getBestSchedule();
	return jss.getBestMakespan();
}

}
