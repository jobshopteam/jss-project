#ifndef JSSGEN_H
#define JSSGEN_H

#include "JssGenLibGlobal.h"
#include "JssTabuBack.h"
#include "JssTabuGen.h"
#include "JssGenParams.h"

namespace jss {

class JSS_GEN_LIB_SHARED_EXPORT JssGen : public JssBase {

public:
	typedef std::vector<double> Chromosome;

private:
	// crossover probability (probability of tossing head, i.e. choosing gene from parent_1)
	double p_cross;
	int noOfElites;
	int noOfMutants;
	int noOfCrossovers;
	// current generation
	int gen;
	// maximum genrations (stopping criteria)
	int max_g;
	// population size
	int pop_size;
	// chromosome size
	int chr_size;

	Chromosome best_chromosome;

	std::vector<std::pair<Chromosome,int>* > popData[2];  // chromosomes + fitnesses
	int population, tmpPop;

protected:
	JssTabuParams tabuParams;
	Schedule s_data[2];
	int bestSchedule, tmpSchedule;

	int best_fitness;

public:
	JssGen(std::istream &is, InputType inputType, JssGenParams params, JssTabuParams tabuParams = JssTabuParams());
	virtual ~JssGen();
	const Chromosome& run();

	// vraca true ako nije dosao do zadnje generacije te provede jedan korak genetskog algoritma
	// inace vraca false
	bool nextGeneration();
	int getBestMakespan() { return best_fitness; }
	Schedule getBestSchedule() { return s_data[bestSchedule]; }
	const Chromosome& getBestChromosome() const { return best_chromosome; }

private:
	virtual JssTabuGen decode(const Chromosome& c) const;


	virtual int fitness(const Chromosome &c, Schedule &schedule) const;
	int fitness1(const Chromosome &c, Schedule &schedule) const;
	int fitness2(const Chromosome &c, Schedule &schedule) const;

	void generateInitialPopulation();
	void evaluatePopulation();

	const Chromosome& kTournament(int k);

};

}
#endif // JSSGEN_H
