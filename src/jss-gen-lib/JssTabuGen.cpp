#include <functional>     // std::greater
#include <algorithm>
#include <map>
#include <queue>          // std::priority_queue
#include <iostream>

#include "JssTabuGen.h"

namespace jss {

int MachineAvailibility::getEarliest(int from, int duration) {
	int N = t.size();

	// prazan vektor ili gledamo nakon zadnjeg
	if (N == 0 || from > t[N-1]) {
		t.push_back(from);
		t.push_back(from + duration);
		return from;
	}

	if (from == t[N-1]) {
		t[N-1] += duration;
		return from;
	}

	// start je najmanji indeks t.d. je t[start] > from
	// takav sigurno postoji
	int start = std::upper_bound(t.begin(), t.end(), from) - t.begin();

	if (start % 2 == 0) {
		// start je pocetak nekog zauzetog intervala
		if (from + duration <= t[start]) {
			// Npr.
			// from = 3, duration = 1
			//      (1,3) , (4,6) zauzeti intervali
			// start = 2-----^
			// stane u prethodnu rupu pa imamo
			// (1,3), (3,4), (4,6) -> (1, 6)

			// stane u prethodu rupu
			// bitan poredak!!
			t.insert(t.begin()+start, from + duration);
			t.insert(t.begin()+start, from);

			// treba li desni rub -> (3,4),(4,6) iz primjera
			if (t[start+1] == t[start+2]) {
				t.erase(t.begin()+start + 1, t.begin() + start + 3);
			}
			// lijevi rub (1,3), (3,6)
			if (start != 0 && t[start-1] == t[start]) {
				t.erase(t.begin()+start-1, t.begin()+start+1);
			}
			return from;
		} else {
			// pomakni se na kraj zauzetog intervala
			++start;
		}
	}

	while (true) {
		if (start == N-1) {
			// overwrite, i.e. add duration
			int ret = t[start];
			t[start] += duration;
			return ret;
		} else {
			if (t[start+1] - t[start] >= duration) {
				int ret = t[start];
				t[start] += duration;
				if (t[start] == t[start+1]) {
					// obrisi elemente na indeksima start i start+1, [begin, end>
					t.erase(t.begin()+start, t.begin() + start + 2);
				}
				return ret;
			} else {
				start += 2;
			}
		}
	}

	return -1; // nikad se ne bi trebalo izvrsiti

}


JssTabuGen::JssTabuGen(const std::vector<double> &c, const std::vector<Job> &_jobList, const int n_machines,
					   JssTabuParams params) : JssTabuBack(params) {

	jobList = _jobList;
	no_of_machines = n_machines;

	int N = c.size() / 2;
	int g = 0;
	int t = 0;

	int MAX_DUR_x_1_5 = getMaxDuration() * 1.5;
	int N_JOBS = jobList.size();

	schedule.resize(N_JOBS);

	std::vector<MachineAvailibility> MA(n_machines);

	int n_scheduled = 0;
	std::priority_queue<int, std::vector<int>, std::greater<int> > F; // finish times

	int j_star;
	int j_star_job;
	int j_star_priority;

	// delay za svaku iteraciju
	std::vector<double> Delay(N);
	int t_Delay;

	// [indeks posla]
	// ->
	// (indeks prve nerasporedjene operacije posla, finish time posljednje rasporedene operacije posla)
	std::vector<std::pair<int, int> > job(N_JOBS);


	for (int i = 0; i < N; i++) {
		Delay[i] = MAX_DUR_x_1_5 * c[N + i];
	}

	for (int i = 0; i < N_JOBS; i++) {
		job[i] = std::make_pair(0, 0);
	}

	F.push(0);

	while (n_scheduled < N) {

		//std::cout << "generacija" << g << std::endl;
		j_star = -1;
		j_star_priority = -1;
		t_Delay = t + Delay[g];

		// nadji j*
		for (int i = 0; i < N_JOBS; i++) {
			if (job[i].first == jobList[i].size()) {
				// rasporedili smo sve operacije ovog posla
				continue;
			}
			if (job[i].second <= t_Delay && (j_star == -1 || c[i] > j_star_priority)) {
				j_star = job[i].first;
				j_star_priority = c[i];
				j_star_job = i;
			}
		}

		if (j_star == -1) {
			// nismo nasli operaciju za rasporediti, tj. skup E je prazan
			while (true) {
				// pomakni vrijeme t
				int t1 = F.top();
				F.pop();
				if (t1 > t) {
					t = t1;
					break;
				}
			}
			continue; // main while loop
		}
		// nasli smo operaciju koju cemo rasporediti
		++n_scheduled;


		Operation op = jobList[j_star_job][j_star];

		int EF_j_star = job[j_star_job].second;

		int F_j_star = MA[op.machine].getEarliest(EF_j_star, op.time) + op.time;

		//std::cout << "nasao earliest" << std::endl;

		F.push(F_j_star);
		++g;

		// ovo hocemo : schedule[j_star_job][j_star] = F_j_star - op.time; // starting time
		schedule[j_star_job].push_back(F_j_star - op.time);

		job[j_star_job] = std::make_pair(j_star + 1, F_j_star);
	}
	// imiritam konstruktor JssTabuBack
	order = convert();
	bestOrder = order;
	bestMakespan = makespan = calculateMakespan(order, schedule);
}


JssTabuGen::JssTabuGen(const std::vector<Job> &jobList, const Schedule &schedule, const JssTabuParams &params) :
	JssTabuBack(params) {
	this->jobList = jobList;
	this->schedule = schedule;
	makespan = 0;
	no_of_machines = 0;
	for (int i = 0; i < jobList.size(); ++i) {
		for (int j = 0; j < jobList[i].size(); ++j) {
			makespan = std::max(makespan, jobList[i][j].time + schedule[i][j]);
			no_of_machines = std::max(no_of_machines, jobList[i][j].machine + 1);
		}
	}
	bestOrder = order = convert();
	bestMakespan = makespan;
	//std::cout << "Done!\n";
}

bool JssTabuGen::localSearch() {
	std::vector<Move> mvs = getMoves(getMachineBlocks(getCriticalPath(schedule, order, 1)[0]));
	Schedule tmp_schedule;
	OperationOrder tmp_order;
	int tmp_makespan;
	// kad nadje _prvi_ potez koji generira bolji makespan onda ga prihvaca i prekida localSearch
	for (int i = 0; i < mvs.size(); ++i) {
		tmp_order = generateOperationOrder(mvs[i]);
		tmp_makespan = calculateMakespan(tmp_order, tmp_schedule);
		if (tmp_makespan < bestMakespan) {
			bestMakespan = makespan = tmp_makespan;
			bestOrder = order = tmp_order;
			schedule = tmp_schedule;
			return true;
		}
	}
	return false;
}

struct sortVectorEntry {
	JssBase::Time time;
	JssBase::Machine machine;
	JssBase::OperationIndex index;

	sortVectorEntry(JssBase::Time t, JssBase::Machine m, JssBase::OperationIndex i){
		time = t;
		machine = m;
		index = i;
	}

	bool operator<(const sortVectorEntry& other) const
	   {
		   return time < other.time;
	   }
};


JssBase::OperationOrder JssTabuGen::convert(){
	OperationOrder ord;
	std::vector<sortVectorEntry> sortVector;

	//punim vektor koji cu sortirati
	for (int i = 0; i < schedule.size(); ++i) {
		for (int j = 0; j < schedule[i].size(); ++j) {

			OperationIndex index(i,j);

			//vrijeme, masina, operation index
			sortVectorEntry entry(schedule[i][j], jobList[i][j].machine, index);

			sortVector.push_back(entry);
		}
	}
	std::sort(sortVector.begin(), sortVector.end());

	ord.resize(no_of_machines);

	//punim ord
	for (int i = 0; i < sortVector.size(); ++i) {
		ord[sortVector[i].machine].push_back(sortVector[i].index);
	}
	return ord;
}

}
