#ifndef JSSTABUGEN_H
#define JSSTABUGEN_H

#include "JssGenLibGlobal.h"
#include "JssTabuBack.h"

#include <vector>

namespace jss {

class JSS_GEN_LIB_SHARED_EXPORT MachineAvailibility {
private:
   std::vector<int> t;
public:
   int getEarliest(int from, int duration);
};

class JSS_GEN_LIB_SHARED_EXPORT JssTabuGen : public JssTabuBack {
private:
	int no_of_machines;

public:
	JssTabuGen(const std::vector<double>& c, const std::vector<Job>& _jobList, const int n_machines,
			   JssTabuParams params);
	JssTabuGen(const std::vector<Job> &jobList, const Schedule &schedule, const JssTabuParams &params);

	bool localSearch();

private:
	OperationOrder convert();
};

}
#endif // JSSTABUGEN_H
