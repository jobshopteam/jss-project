QT -= gui

TARGET = jss-gen-lib
TEMPLATE = lib

DESTDIR = ../../

OBJECTS_DIR = $$DESTDIR/obj/jss-gen-lib
MOC_DIR = $$DESTDIR/moc/jss-gen-lib
RCC_DIR = $$DESTDIR/rcc/jss-gen-lib
UI_DIR = $$DESTDIR/ui/jss-gen-lib

DEFINES += JSS_GEN_LIB_LIBRARY

LIBS += -L$$DESTDIR -ljss-tabu-lib

SOURCES += JssGen.cpp \
	JssTabuGen.cpp \
	JssHybrid.cpp

HEADERS += JssGen.h \
	JssGenLibGlobal.h \
	JssTabuGen.h \
	JssGenParams.h \
	JssHybrid.h

INCLUDEPATH += $$PWD/../jss-tabu-lib
DEPENDPATH += $$PWD/../jss-tabu-lib

unix:QMAKE_CXXFLAGS += -Wno-sign-compare -O3

unix {
	target.path = /usr/lib
	INSTALLS += target
}
