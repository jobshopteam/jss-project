#ifndef _JSS_BASE_H_
#define _JSS_BASE_H_

#include "JssTabuLibGlobal.h"

#include <utility>
#include <vector>
#include <string>
#include <list>
#include <set>

namespace jss {

/**
 * Bazna klasa biblioteke.
 *
 * Definira osnovne objekte u JSS problemu i osnovne metode za rad s njima.
 *
 * mjere za slozenost:
 * > o = |jobList| = sum(|jobList[i]|) = "broj operacija"
 */
class JSS_TABU_LIB_SHARED_EXPORT JssBase {
public:

	/**
	 * Tip ulazne datoteke koji se koristi u JssBase(std::istream&, InputType).
	 */
	enum InputType {
		//nas format inputa, kao u JssBase::JssBase(std::string)
		Custom,
		//taillardov format, prvo matrica vremena, onda matrica strojeva,
		//svaki posao ima tocno 1 operaciju na svakom stroju
		Taillard,
		//demirkolov format, matrica parova strojeva i vremena
		//svaki posao ima tocno 1 operaciju na svakom stroju
		Demirkol
	};

	// Definicije osnovnih tipova
	typedef int Time; ///< Tip koji predstavlja vrijeme potrebno za posao.
	typedef int Machine; ///< Stroj koji obavlja operacije.
	/**
	 * Operacija u jednom poslu.
	 */
	struct Operation {
		Machine machine; ///< Stroj na kojem se operacija obavlja
		Time time; ///< Vrijeme trajanja operacije
		/**
		 * Konstruktor.
		 *
		 * @param m Stroj.
		 * @param t Vrijeme.
		 */
		Operation(Machine m = 0, Time t = 0) : machine(m), time(t) {}
	};

	/**
	 * Tip koji predstavlja posao, vektor operacija koje se trebaju obaviti
	 * u redosljedu kojem su spremljene.
	 */
	typedef std::vector<Operation> Job;
	/**
	 * Indeks operacije, prvi element je indeks posla, a drugi operacije u tom
	 * poslu.
	 */
	struct OperationIndex {
		int job; ///< Indeks posla.
		int op; ///< Indeks operacije u poslu.
		OperationIndex(int job = -1, int op = -1) : job(job), op(op) {}
		/**
		 * Operator jednakosti.
		 * @return true ako su i stroj i operacija oba argumenta jednaki.
		 */
		friend bool operator==(const OperationIndex &i, const OperationIndex &j) {
			return i.job == j.job && i.op == j.op;
		}
		/**
		 * Operator usporedjivanja.
		 */
		friend bool operator<(const OperationIndex &i, const OperationIndex &j) {
			return i.job < j.job || (i.job == j.job && i.op < j.op);
		}
	};

	/**
	 * Poredak na jednom stroju.
	 */
	typedef std::vector<OperationIndex> MachineOperationOrder;
	/**
	 * Potpuni poredak.
	 */
	typedef std::vector<MachineOperationOrder> OperationOrder;
	/**
	 * Raspored poslova. Na j-tom mjestu i-tog vektora zapisano je vrijeme
	 * pocetka obavljanja j-te operacije i-tog posla.
	 */
	typedef std::vector<std::vector<Time> > Schedule;

	typedef std::vector<OperationIndex> Path; ///< Put u disjunktivnom grafu.

	typedef std::pair<OperationIndex, OperationIndex> Move;

	// varijable
protected:
	std::vector<Job> jobList; ///< Vektor poslova trenutnog problema.
	OperationOrder order; ///< Trenutni poredak operacija na strojevima.
	Schedule schedule; ///< Trenutni raspored poslova.
	Time makespan; ///< Trenutni makespan.

	// metode
protected:
	JssBase(){}
public:
	/**
	 * Konstruktor koji ucitava problem s input stream-a `is`. Format je odredjen s `inputType`.
	 *
	 * @param is Stream s kojeg se ucitava instanca problema
	 * @param inputType Tip inputa
	 */
	JssBase(std::istream& is, InputType inputType = Custom);

	/**
	 * Postavlja trenutni poredak operacija.
	 */
	void setOperationOrder(const OperationOrder &order) {
		this->order = order;
		makespan = calculateMakespan(order, schedule);
	}
	/**
	 * Vraca raspored za trenutni poredak operacija.
	 */
	const Schedule& getSchedule() {
		return schedule;
	}

	/**
	 * Vraca vektor poslova problema.
	 */
	const std::vector<Job>& getJobList() const {
		return jobList;
	}

	/**
	 * Vraca makespan za trenutni poredak operacija
	 *
	 * slozenost: O(o) ako raspored nije jos izracunat, O(1) inace
	 */
	Time getMakespan() {
		return makespan;
	}

	/**
	 *
	 */
	OperationOrder getOrder() {
		return order;
	}

	Path getCriticalPath() {
		return getCriticalPath(schedule, order)[0];
	}

protected:
	/**
	 * Funkcija racuna makespan i raspored za dani poredak operacija.
	 *
	 * @param order Poredak operacija po strojevima
	 * @param schedule Objekt u koji ce se zapisati raspored operacija po
	 *        strojevima.
	 *
	 * @return Makespan za dani raspored.
	 *
	 * slozenost: O(o)
	 *
	 * TODO: baciti iznimku ako je redosljed operacija neizvediv
	 *       (graf ima ciklus)
	 */
	Time calculateMakespan(const OperationOrder &order, Schedule &schedule) const;

	/**
	 * Funkcija racuna kriticne puteve u grafu.
	 *
	 * @param schedule Raspored za koji se racuna kriticni put
	 * @param order Poredak operacija s kojim je izracunat schedule
	 * @param limit Maksimalan broj kriticnih puteva koje treba vratiti
	 *
	 * @return vektor pronadjenih kriticnih puteva
	 *
	 * TODO: Za sad funkcija vraca samo 1 rezultat bez obzira na limit, implementirati trazenje vise puteva.
	 *
	 * slozenost: O(o)
	 */
	std::vector<Path> getCriticalPath(const Schedule &schedule,
			const OperationOrder &order, int limit = 1) const;

	/**
	 * Vraca blokove operacija koje se izvode na istoj masini.
	 */
	std::vector<Path> getMachineBlocks(Path& criticalPath);

	/**
	 * Dohvaca masinu na kojoj se izvodi dana operacija
	 */
	Machine getMachine(const OperationIndex& oi) const {
		return jobList[oi.job][oi.op].machine;
	}

	OperationOrder convert(Schedule schedule);

	int getNumberOfMachines() const { return order.size(); }
	int getNumberOfOperations() const;
	int getMaxDuration() const;

private:
	void sanityCheck() const;
};

}

#endif //_JSS_BASE_H_
