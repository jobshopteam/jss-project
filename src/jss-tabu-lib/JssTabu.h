#ifndef _JSS_TABU_H_
#define _JSS_TABU_H_

#include "JssTabuLibGlobal.h"

#include "JssBase.h"

namespace jss {

class JSS_TABU_LIB_SHARED_EXPORT JssTabu : public JssBase {

protected:

	class TabuList {
	public:
		typedef std::vector<Move>::size_type size_type;
	private:
		std::vector<Move> data;
		std::multiset<Move> moves;

		int head;
	public:
		TabuList(int size = 0) : data(size, Move(0, 0)), head(0) {}
		/**
		 * Ubacuje dani potez na kraj tabu liste te pritom izbacuje i vraca potez s pocetka tabu liste.
		 */
		Move insert(const Move &move);
		/**
		 * Vraca zadnji element liste.
		 */
		const Move& back() const {
			return data[(head + data.size() - 1) % data.size()];
		}
		/**
		 * Vraca `k`-ti element liste.
		 */
		const Move& operator[](int k) const {
			return data[(head + k) % data.size()];
		}
		/**
		 * Vraca velicinu liste.
		 */
		size_type size() const {
			return data.size();
		}
		/**
		 * Vraca true ako je `move` u listi, a inace false.
		 */
		bool contains(const Move &move) {
			return moves.count(move) > 0;
		}
	};

	static const int defaultTabuMax = 20;
	static const int defaultIterMax = 1500;

	const int tabuMax;
	const int iterMax;

	TabuList tabuList;

protected:
	JssTabu(int _tabuMax = defaultTabuMax, int _iterMax = defaultIterMax) : JssBase(), tabuMax(_tabuMax), iterMax(_iterMax), tabuList(_tabuMax){}

public:

	/**
	 * Konstruktor koji ucitava problem s input stream-a `is`. Format je odredjen s `inputType`.
	 *
	 * @param is Stream s kojeg se ucitava instanca problema
	 * @param inputType Tip inputa
	 */
	JssTabu(std::istream& is, InputType inputType = Custom,
			int tabuMax = defaultTabuMax, int iterMax = defaultIterMax)
		: JssBase(is, inputType), tabuMax(tabuMax), iterMax(iterMax), tabuList(tabuMax) {}

protected:

	/**
	 * Vraca sve poteze iz trenutnog rasporeda.
	 */
	std::vector<Move> getMoves(const std::vector<Path>& blocks);

	/**
	 * Iz trenutnog poretka generira novi tako da primijeni move (v. Clanak Q(pi, v))
	 */
	OperationOrder generateOperationOrder(const Move &move) const;

	/**
	 * Klasificira proslijedjene poteze u one dozvoljene i one isplative i nedozvoljene.
	 *
	 * Takoder izracuna novi makespan za te poteze.
	 *
	 * @param moves Proslijedjeni skup poteza.
	 * @param permitedMoves Vektor koji ce metoda popuniti dozvoljenim potezima.
	 * @param permitedMakespans Vektor koji ce metoda popuniti makespanima za svaki od dozvoljenih poteza.
	 * @param forbiddenProfitableMoves Vektor koji ce metoda popuniti nedozvoljenim isplativim potezima.
	 * @param forbiddenMakespans Vektor koji ce metoda popuniti makespanima nedozvoljenih isplativih poteza.
	 */
	void classifyMoves(const std::vector<Move> &moves, std::vector<Move> &permitedMoves,
					   std::vector<Time> &permitedMakespans, std::vector<Move> &forbiddenProfitableMoves,
					   std::vector<Time> &forbiddenMakespans);

};

}
#endif //_JSS_TABU_H_
