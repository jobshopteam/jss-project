QT -= gui

TARGET = jss-tabu-lib
TEMPLATE = lib

DESTDIR = ../../

OBJECTS_DIR = $$DESTDIR/obj/jss-tabu-lib
MOC_DIR = $$DESTDIR/moc/jss-tabu-lib
RCC_DIR = $$DESTDIR/rcc/jss-tabu-lib
UI_DIR = $$DESTDIR/ui/jss-tabu-lib

DEFINES += JSS_TABU_LIB_LIBRARY

SOURCES += \
	JssBase.cpp \
	JssTabu.cpp \
	JssTabuBack.cpp

HEADERS +=\
	JssBase.h \
	JssTabu.h \
	JssTabuBack.h \
	JssTabuLibGlobal.h \
	JssTabuParams.h

unix:QMAKE_CXXFLAGS += -Wno-sign-compare -O3

unix {
	target.path = /usr/lib
	INSTALLS += target
}
