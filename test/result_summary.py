#!/usr/bin/python

import os, re, sys

if len(sys.argv) > 2:
    print "Usage: {} [color|nocolor]".format(sys.argv[0]) 
    sys.exit()

def isTest(dirname):
    return re.match(r'test-2015-.*', dirname) is not None


testdirs = sorted(filter(isTest, os.listdir('.')))
result_files = ['taillard', 'demirkol']
result_file_suffix = '_lu_bounds.txt'

if len(sys.argv) == 1 or sys.argv[1] != 'nocolor':
    colors = ['\x1b[31;1m', '\x1b[33;1m', '\x1b[32;1m']
    color_reset = '\x1b[0m'
else:
    colors = ['', '', '']
    color_reset = ''

def getColor(makespan, up):
    if makespan < 0:
        return ""
    if makespan > ub:
        return colors[0]
    elif makespan < ub:
        return colors[2]
    else:
        return colors[1]

print 'INPUT FILE\tlower_bound\tupper_bound\t' + '\t'.join(testdirs)

avg = [0.0] * len(testdirs)

for rfile in result_files:
    rf = open(rfile + result_file_suffix)
    for line in rf:
        tmp = line.strip('\t\n ').split('\t')
        filename, lb, ub = tmp[0], int(tmp[1]), int(tmp[2])
        #print "filename = {}, lb = {}, ub = {}".format(filename, lb, ub)
        makespans = []
        for testdir in testdirs:
            testfile = open('{}/{}/{}.txt.out'.format(testdir, rfile, filename))
            try: 
                makespans.append(int(re.search(r'MAKESPAN=(\d+)', testfile.read()).group(1)))
            except:
                makespans.append(-1)
                print >> sys.stderr, '{}/{}/{}.txt.out'.format(testdir, rfile, filename)
            testfile.close()
        print '{}\t{}\t{}\t'.format(filename, lb, ub) + '\t'.join('{}{}({:+.2f}){}'.format(getColor(makespan, ub), makespan, float(makespan - ub) / ub, color_reset) for makespan in makespans)
        avg = [s + float(n - ub) / ub if n > 0 else s for s, n in zip(avg, makespans)]

print "total\t\t\t" + "\t".join('{:+.2f}'.format(s / 160.0) for s in avg)

